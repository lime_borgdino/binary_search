/*
    Binary Search Notes

    - Depends on input being pre-sorted array
    - Average time complexity is reduced to O(log n) 
    vs linear search O(n)
    - Narrows search space by half each time it compares
    key value to middle value

*/

function binarySearch(a,x,start,end) {
    if (start > end) {
        return false
    }

    let mid = Math.floor((start + end) / 2)

    if (a[mid] == x) {
        return true
    }

    if (a[mid] > x) {
        return binarySearch(a,x,start,mid-1)
    } else {
        return binarySearch(a,x,mid+1,end)
    }
}

let a = [2,3,5,7,11,13]
console.log(binarySearch(a,5,0,a.length-1));        // return true
console.log(binarySearch(a,6,0,a.length-1));        // return false
